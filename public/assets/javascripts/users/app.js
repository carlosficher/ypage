(function ($) {

    'use strict';

    var lastAddress = null, ElementoClonado = null, newId = null, h3 = null, n = null, i = null;
    
    var adicionarEndereco = () => {
        
        if (lastAddress === null) {
            lastAddress = document.querySelector(".all-address div:last-child");
        } 
        
        ElementoClonado = $(lastAddress).clone();
        newId = parseInt($(ElementoClonado).attr('id')) + 1;
        h3 = $(ElementoClonado).children().children('h3');
        var textH3 = $(h3).html().split(' ');
        n = parseInt(textH3[1]) + 1;
        i = parseInt(textH3[1]);                
        var inputs = $(ElementoClonado).children().siblings();
        
        $(inputs).each(function(e) {
            var name = $(this).children('div').children('input').attr('name');
            $(this).children('div').children('input').val('');
            
            if (name != undefined) {
                name = name.replace(i - 1, i);
                $(this).children('div').children('input').attr('name', name)
            }
        });
        
        $(h3).html('Endereço ' + n);
        
        $(ElementoClonado).attr('id', newId);

        $(ElementoClonado).appendTo('.all-address');
        
        lastAddress = ElementoClonado;
        $('.remove-address').on("click", removerEndereco);
        $(this).unbind("click");
    }

    var removerEndereco = () => {
        $(this.parentNode).remove();
    }

    $('.add-address').on("click", adicionarEndereco);
    $('.remove-address').on("click", removerEndereco);
    
    /*
     Wizard #user
     */
    var $w3finish = $('#user').find('ul.pager li.finish'),
            $w3validator = $("#user form").validate({
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).remove();
        },
        errorPlacement: function (error, element) {
            element.parent().append(error);
        }
    });

    $w3finish.on('click', function (ev) {
        ev.preventDefault();
        var validated = $('#user form').valid();
        if (validated) {
            var form = $('#user form').serialize();
            
            var $url = $('#user form').attr('action');

            $.ajax({
                type: "POST",
                url: $url,
                data: form,
                success: function (s) {
                    location.reload();
                },
                error: function (e) {
                    console.log(e.responseJSON.errors);
                    var errors = '';
                    for (var k in e.responseJSON.errors ){
                        console.log(e.responseJSON.errors[k]);
                        var lineError = e.responseJSON.errors[k] + '<br>';
                        errors += lineError;
                    }
                    
                    var alert = $('.alert-danger');
                    $(alert).find('span').html(errors);
                    $(alert).removeClass('hidden');
                    $('.mfp-wrap').animate({
                        scrollTop: $(alert).offset().top
                    });
                    
                }
            });
            
            console.log(form);
//            new PNotify({
//                title: 'Congratulations',
//                text: 'You completed the wizard form.',
//                type: 'custom',
//                addclass: 'notification-success',
//                icon: 'fa fa-check'
//            });
        }
    });

    $('#user').bootstrapWizard({
        tabClass: 'wizard-steps',
        nextSelector: 'ul.pager li.next',
        previousSelector: 'ul.pager li.previous',
        firstSelector: null,
        lastSelector: null,
        onNext: function (tab, navigation, index, newindex) {
            var validated = $('#user form').valid();
            if (!validated) {
                $w3validator.focusInvalid();
                return false;
            }
        },
        onTabClick: function (tab, navigation, index, newindex) {
            if (newindex == index + 1) {
                return this.onNext(tab, navigation, index, newindex);
            } else if (newindex > index + 1) {
                return false;
            } else {
                return true;
            }
        },
        onTabChange: function (tab, navigation, index, newindex) {
            var $total = navigation.find('li').length - 1;
            $w3finish[ newindex != $total ? 'addClass' : 'removeClass' ]('hidden');
            $('#user').find(this.nextSelector)[ newindex == $total ? 'addClass' : 'removeClass' ]('hidden');
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length - 1;
            var $current = index;
            var $percent = Math.floor(($current / $total) * 100);
            $('#user').find('.progress-indicator').css({'width': $percent + '%'});
            tab.prevAll().addClass('completed');
            tab.nextAll().removeClass('completed');
        }
    });


}).apply(this, [jQuery]);