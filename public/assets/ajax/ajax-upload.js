
var Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.doUpload = function () {   
    var progress_bar_id = "#progress-wrp";
    
    $(progress_bar_id).removeClass('hidden');
    $(progress_bar_id + " .progress-bar").css("width", "0%");
    $(progress_bar_id + " .status").text("0%");
    
    var that = this;
    var formData = new FormData();

    formData.append("file", that.file);
    formData.append("upload_file", true);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });

    $.ajax({
        type: "POST",
        url: $url,
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            $('.uploaded').val(data.filename);
            $('.img-uploaded img').attr('src', data.filename);
        },
        error: function (error) {
            $(progress_bar_id).addClass('hidden');
            new PNotify({
                title: titleError + '!',
                text: textError + '. ' + error.responseJSON.message,
                type: 'error'
            });
            $('.img-uploaded img').attr('src', imgError);
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
};

$(".upload").on("click", function(e) {
    $(".fileupload").trigger('click');
});

$(".fileupload").on("change", function (e) {
    var upload = new Upload($(this)[0].files[0]);

    // maby check size or type here with upload.getSize() and upload.getType()

    // execute upload
    upload.doUpload();
});