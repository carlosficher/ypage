<?php

namespace App\Http\Controllers\Web;

use App\Models\Product;
use App\Models\ProductAward;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:product-list');
         $this->middleware('permission:product-create', ['only' => ['create','store']]);
         $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['bonus_p', 'bonus_d'])->latest()->paginate(5);
        return view('admin.products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'price' => 'required'
        ]);


        try {
            $product = Product::create($request->all());

            $bonusP = $request->input('bonus_p');
            foreach ($bonusP as $k => $v) {
                if ($k !== 'calc_type') {
                    $productAward['product_id'] = $product->id;
                    $productAward['value'] = $v;
                    $productAward['calc_type'] = $bonusP['calc_type'];
                    $productAward['type'] = 'p';
                    $productAward['level'] = $k;
                    $productAward['is_active'] = $request->input('is_active');
                    ProductAward::create($productAward);
                }                
            }
            
            $bonusD = $request->input('bonus_d');
            foreach ($bonusD as $k => $v) {
                if ($k !== 'calc_type') {
                    $productAward['product_id'] = $product->id;
                    $productAward['value'] = $v;
                    $productAward['calc_type'] = $bonusD['calc_type'];
                    $productAward['type'] = 'd';
                    $productAward['level'] = $k;
                    $productAward['is_active'] = $request->input('is_active');
                    ProductAward::create($productAward);
                }                
            }


            $image = $request->input('photo');
            
            if (!empty($image)) {
                $ext = explode('.', $image);

                $productPath = '/uploads/product/';
                $newName = $productPath . $product->id . '.' . $ext[1];

                $destinationPath = public_path($productPath);
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath);
                }

                $newDirectory = $destinationPath . $product->id . '.' . $ext[1];
                rename(public_path($image), $newDirectory);

                Product::find($product->id)->update(['photo' => $newName]);
            }
            
            return redirect()->route('products.index')
                            ->with('success', trans('general.product_created_successfully'));
            
        } catch (Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }            
        
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.products.show',compact('product'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::with(['bonus_p', 'bonus_d'])->find($product->id);

        return view('admin.products.edit',compact('product'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
         request()->validate([
            'name' => 'required',
            'detail' => 'required',
            'price' => 'required',
             'bonus_d.calc_type' => 'required',
             'bonus_d' => 'required',
        ]);

        try {
                     
            $product->update($request->all());
            
            $bonusP = $request->input('bonus_p');
            ProductAward::where('product_id', $product->id)->first()->delete();
            foreach ($bonusP as $k => $v) {
                if ($k !== 'calc_type') {
                    $productAward['product_id'] = $product->id;
                    $productAward['value'] = $v;
                    $productAward['calc_type'] = $bonusP['calc_type'];
                    $productAward['type'] = 'p';
                    $productAward['level'] = $k;
                    $productAward['is_active'] = $request->input('is_active');
                    ProductAward::create($productAward);
                }                
            }
            
            $bonusD = $request->input('bonus_d');
            foreach ($bonusD as $k => $v) {
                if ($k !== 'calc_type') {
                    $productAward['product_id'] = $product->id;
                    $productAward['value'] = $v;
                    $productAward['calc_type'] = $bonusD['calc_type'];
                    $productAward['type'] = 'd';
                    $productAward['level'] = $k;
                    $productAward['is_active'] = $request->input('is_active');
                    ProductAward::create($productAward);
                }                
            }

            
            $image = $request->input('photo');            
            
            if (!empty($image)) {
                                
                $explode = explode('product/', $image);
                        
                if (is_file(public_path('uploads/temp/product/' . $explode[1]))) {
                    $ext = explode('.', $image);                

                    $productPath = '/uploads/product/';


                    $newName = $productPath . $product->id . '.' . $ext[1];

                    $destinationPath = public_path($productPath);
                    if (!is_dir($destinationPath)) {
                        mkdir($destinationPath);
                    }

                    $newDirectory = $destinationPath . $product->id . '.' . $ext[1];
                    rename(public_path($image), $newDirectory);

                    Product::find($product->id)->update(['photo' => $newName]);
                }
            }

            return redirect()->route('products.index')
                        ->with('success', trans('general.product_updated_successfully'));
            
             
         } catch (Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
         }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();


        return redirect()->route('products.index')
                        ->with('success','Product deleted successfully');
    }
    
    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/temp/product');

            try {
                $image->move($destinationPath, $name);
                return ['filename' => '/uploads/temp/product/' . $name];
            } catch (Exception $ex) {
                return $ex;
            }
            
        }
    }
}
