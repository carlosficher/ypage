@extends('layouts.app')

@section('page_heading', trans('general.management.edit_role'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('roles.index')}}">
                {{trans('general.management.roles')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>{{trans('general.whoops')}}!</strong> {{trans('general.text_whoops')}}.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><i class="icon icon-layers"></i></h2>
    </header>
    <div class="panel-body">
        {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>{{trans('general.name')}}:</strong>
                    {!! Form::text('name', null, array('placeholder' => trans('general.name'),'class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="form-group">
                    <strong>{{trans('general.display_name')}}:</strong>
                    {!! Form::text('display_name', null, array('placeholder' => trans('general.display_name'),'class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.description')}}:</strong>
                    {!! Form::textarea('description', null, array('placeholder' => trans('general.description'),'class' => 'form-control','style'=>'height:100px')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <strong>{{trans('general.permission')}}:</strong>
            </div>
        </div>
        <div class="row">
            @foreach($permission as $value)    
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="form-group">
                    <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                	{{ $value->display_name }}
                    </label>
                </div>
            </div>
            @endforeach
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</section>
@endsection