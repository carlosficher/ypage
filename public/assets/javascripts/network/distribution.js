var getId = null;
    
var cadastro = null;

(function ($) {

    'use strict';
    
    
    /*
     Modal with CSS animation
     */
    
    $(document).on('click', '.modal-user', function () {
        $(this).magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});
        $(this).trigger('click');
    });
    
    
    /*
     Modal Dismiss
     */
    $(document).on('click', '.modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    /*
     Modal Confirm
     */
    $(document).on('click', '.modal-confirm', function (e) {
        e.preventDefault();
        deleteRegister();
    });

    var displayOverlay = (text) => {
        $("<table id='overlay'><tbody><tr><td>" + text + "</td></tr></tbody></table>").css({
            "position": "fixed",
            "top": 0,
            "left": 0,
            "width": "100%",
            "height": "100%",
            "background-color": "rgba(0,0,0,.5)",
            "z-index": 10000,
            "vertical-align": "middle",
            "text-align": "center",
            "color": "#fff",
            "font-size": "30px",
            "font-weight": "bold",
            "cursor": "wait"
        }).appendTo("body");
    }


//consultando itens
    var items = [], itemConfig = [], link = null;

    getId = (id) => {
        arvore(id);
    }

    cadastro = (parentId, position) => {       
        $('#parent').val(parentId);
        $('#position').val(position);
    }

    var arvore = (userId) => {
        displayOverlay('Carregando Dados. Por Favor Aguarde...');
        items = [];
        var getUrl = base_url + '/network/distribution?user=' + userId;
        $.ajax({
            type: 'GET',
            url: getUrl,
            cache: true,
            async: false,
            contentType: "application/json",
            dataType: 'json',
            success: function (data)
            {

                $.each(data, function (index, val) {
                    items.push(
                            new primitives.orgdiagram.ItemConfig({
                                icon: index == 0 ? 'fa fa-arrow-circle-up fa-2x' : 'fa fa-sitemap fa-2x',
                                iconTitle: index == 0 && val.name ? 'Subir um nível' : (index != 0 && val.name ? 'Carregar rede a partir deste usuário' : 'Cadastrar novo afiliado'),
                                id: val.name ? val.id : val.parent_id + 'p' + val.position_l,
                                action: index == 0 && val.name ? 'getId(' + val.parent_id + ')' : (index != 0 && val.name ? 'getId(' + val.id + ')' : 'cadastro(' + val.parent_id + ', ' + val.position_l + ')'),
                                parent: val.parent_id,
                                title: val.name,
                                description: "cadastro/#" + val.parent_id + '?p=' + val.position_l,
                                phone: val.phone,
                                email: val.email,
                                image: val.thumb,
                                cssProfileView: val.name ? 'inherit' : 'none',
                                cssProfileNew: val.name ? 'none' : 'inherit',
                                cssUlDetalhes: val.name ? 'visible' : 'hidden',
                                itemTitleColor: statusCor(val.is_active),
                            })
                            );

                });
                geraArvore();

                $("#overlay").remove();
            },
            error: function (request, status, erro) {
                console.log("Problema ocorrido: " + status + "\nDescrição: " + erro);
                $("#overlay").remove();
            }
        });

    }
    ;
//final consulta itens
    var statusCor = function (sts) {
        if (sts === 0) {
            return 'red';
        } else if (sts == 1) {
            return 'green';
        } else if (sts == 2) {
            return 'blue';
        } else {
            return '#a904ea';
        }
    };

    var geraArvore = () => {
        var options = new primitives.orgdiagram.Config();
        options.items = items;
        options.cursorItem = 0;
        options.templates = [getContactTemplate()];
        options.onItemRender = onTemplateRender;
        options.defaultTemplateName = "contactTemplate";
        options.normalItemsInterval = 10;
        options.normalLevelShift = 44;
        $("#basicdiagram").orgDiagram(options);
        function onTemplateRender(event, data) {
            switch (data.renderingMode) {
                case primitives.common.RenderingMode.Create:
                    /* Initialize widgets here */
                    break;
                case primitives.common.RenderingMode.Update:
                    /* Update widgets here */
                    break;
            }

            itemConfig = data.context;

            if (data.templateName == "contactTemplate") {
                data.element.find("[name=photo]").attr({"src": itemConfig.image, "alt": itemConfig.title});
                data.element.find("[name=photo]").css({"border": '3px solid ' + itemConfig.itemTitleColor});
                data.element.find("[name=button]").attr({"class": itemConfig.icon});
                data.element.find("[name=ul-detalhes]").css({"visibility": itemConfig.cssUlDetalhes});
                data.element.find("[name=profile_view]").css({"display": itemConfig.cssProfileView});
                data.element.find("[name=profile_new]").css({"display": itemConfig.cssProfileNew});
                data.element.find("[name=id]").attr({"id": itemConfig.id, "title": itemConfig.iconTitle});
                data.element.find("[name=id]").attr({"data-parent": itemConfig.parent});
                data.element.find("[name=id]").attr({"onclick": itemConfig.action});
                var fields = ["title", "description", "phone", "email"];

                for (var index = 0; index < fields.length; index++) {
                    var field = fields[index];
                    var element = data.element.find("[name=" + field + "]");
                    //if (element.text() != itemConfig[field]) {
                    element.text(' ' + itemConfig[field]);
                    //}
                }

            }

        }

        function getContactTemplate() {
            var result = new primitives.orgdiagram.TemplateConfig();
            result.name = "contactTemplate";
            result.itemSize = new primitives.common.Size(240, 161);
            result.minimizedItemSize = new primitives.common.Size(20, 20);
            result.highlightPadding = new primitives.common.Thickness(4, 4, 4, 4);
            var itemTemplate = $(
                    '  <div  name="titleBackground" class="bp-item bp-corner-all bt-item-frame profile_details">'
                    + '    <div class="well profile_view" name="profile_new">'
                    + '        <div class="col-xs-12 text-center" style="border-bottom: solid 2px #f2f2f2;padding-bottom: 5px;">'
                    + '            <div class="col-xs-3"></div>'
                    + '            <div class="col-xs-6 emphasis">'
                    + '                <a name="id" class="btn btn-new btn-sm modal-user" href="#modalUser" onclick="cadastro(this)" title="">'
                    + '                    <i class="fa fa-plus fa-3x"> </i>'
                    + '                </a>'
                    + '            </div>'
                    + '            <div class="col-xs-3"></div>'
                    + '        </div>'
                    + '    </div>'
                    + '    <div class="well profile_view" name="profile_view">'
                    + '        <div class="col-xs-12 text-center" style="border-bottom: solid 2px #f2f2f2;padding-bottom: 10px;">'
                    + '            <div class="col-xs-3"></div>'
                    + '            <div class="col-xs-6">'
                    + '                <img name="photo" alt="" class="img-circle img-responsive" style="width: 75%">'
                    + '            </div>'
                    + '            <div class="col-xs-2 emphasis">'
                    + '                <button name="id" data-parent="" type="button" class="btn btn-info btn-xs" onclick="getId(this)" title="">'
                    + '                    <i name="button" class=""> </i>'
                    + '                </button>'
                    + '            </div>'
                    + '        </div>'
                    + '        <div class="col-xs-12 bottom text-center">'
                    + '            <div class="col-xs-12 emphasis">'
                    + '                <h5 name="title" class="brief"><i></i></h5>'
                    + '            </div>'
                    + '        </div>'            
                    + '        <div class="col-xs-10">'
                    + '            <ul class="list-unstyled" name="ul-detalhes">'
                    + '                 <li class="hidden"><i class="fa fa-link"></i><strong> <span name="description" onclick="cadastro(this)"></span></strong></li>'
                    + '                 <li><i class="fa fa-envelope"></i> <span name="email" style="font-size: 10px;text-transform: lowercase;"></span></li>'
                    + '                 <li><i class="fa fa-phone"></i> <span name="phone"></span></li>'
                    + '            </ul>'
                    + '        </div>'
                    + '    </div>'
                    + '</div>'
                    ).css({
                width: result.itemSize.width + "px",
                height: result.itemSize.height + "px"
            }).addClass("bp-item bp-corner-all bt-item-frame");

            result.itemTemplate = itemTemplate.wrap('<div>').parent().html();

            return result;
        }

    }


    arvore(userId);

}).apply(this, [jQuery]);    
