<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    protected $fillable = [
        'user_id',
        'bank_id',
        'agency',
        'account',
        'account_type',
        'holder_document',
        'holder_name',
    ];
}
