@extends('layouts.app')

@section('page_heading', trans('general.management.products'))

@section('stylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@stop

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@can('product-create')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-default text-success" href="{{ route('products.create') }}" title="{{ trans('general.management.new_product') }}"><i class="icon icon-plus fa-2x"></i> </a>            
        </div>
    </div>
</div>
@endcan

<!-- start: page -->
<section class="panel">
    <div class="panel-body">
        <div class="result-data">
            <table class="table table-bordered table-striped mb-none"  id="datatable-default" data-url="{{ route('products.index') }}" >
                <thead>
                    <tr>
                        <th width="100px">{{trans('general.image')}}</th>
                        <th>ID#</th>
                        <th>{{trans('general.name')}}</th>
                        <th>{{trans('general.price')}}</th>
                        <th>{{trans('general.bonus_p')}}</th>
                        <th>{{trans('general.bonus_d')}}</th>                        
                        <th>{{trans('general.active')}}</th>
                        <th width="170px">{{trans('general.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $key => $product)
                    <tr>
                        <td class="text-center"><img class="img-responsive" src="{{asset($product->photo)}}"></td>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>R${{ number_format($product->price, 2, ',', '.') }}</td>
                        <td>
                            @foreach( $product->bonus_p as $bonus_p )
                            <p>
                                <b>{{trans('general.level'.$bonus_p->level)}}</b>
                                    {{$bonus_p->calc_type == 'a' ? 'R$' . number_format($bonus_p->value, 2, ',', '.') : 'R$' . number_format($bonus_p->value/100 * $product->price, 2, ',', '.')}}
                            </p>
                            @endforeach
                        </td>
                        <td>
                            @foreach( $product->bonus_d as $bonus_d )
                            <p>
                                <b>{{trans('general.level'.$bonus_d->level)}}</b>
                                    {{$bonus_d->calc_type == 'a' ? 'R$' . number_format($bonus_d->value, 2, ',', '.') : 'R$' . number_format($bonus_d->value/100 * $product->price, 2, ',', '.')}}
                            </p>
                            @endforeach
                        </td>                        
                        <td>{!! $product->is_active == 0 ? '<span class="tag label label-default">Não</span>' : '<span class="tag label label-success">Sim</span>' !!} </td>
                        <td>
                            <a class="btn text-info" href="{{ route('products.show',$product->id) }}" title="{{trans("general.management.show_product")}}"><i class="icon icon-info"></i></a>
                            @can('product-edit')
                            <a class="btn text-warning" href="{{ route('products.edit',$product->id) }}" title="{{trans("general.management.edit_product")}}"><i class="icon icon-note"></i></a>
                            @endcan
                            @can('product-delete')
                            <a class="btn text-danger modal-with-zoom-anim" href="#modalConfirm" onclick="deleteConfirm('{{$product->id}}', this)" title="{{trans("general.management.delete_product")}}"><i class="icon icon-trash"></i></a>                            
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $products->render() !!}
        </div>
    </div>
</section>

@include('components/modal-confirm')

@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@stop

@section('javascripts')
<script src="{{asset('assets/javascripts/tables/examples.datatables.default.js')}}"></script>
@stop