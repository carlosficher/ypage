@extends('layouts.login')

@section('content')

<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="{{url('/')}}" class="logo pull-left">
            <img src="{{asset('assets/images/logo.png')}}" height="54" alt="{{config('app.name')}}" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> {{trans('general.recover_password')}}</h2>
            </div>
            <div class="panel-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @else
                <div class="alert alert-info">
                    <p class="m-none text-weight-semibold h6">{{trans('general.text_info_recover_password')}}!</p>
                </div>                
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group mb-none">
                        <div class="input-group">
                            <input id="email" type="email" class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-lg" type="submit">{{trans('general.reset')}}!</button>
                            </span>
                        </div>
                    </div>

                    <p class="text-center mt-lg">{{trans('general.remembered')}}? <a href="{{route('login')}}">{{trans('general.signin')}}!</a></p>
                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright {{date('Y')}}. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

@endsection
