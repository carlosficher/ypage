@extends('layouts.app')

@section('modalconfirmstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
@stop

@section('page_heading', trans('general.management.settings'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>{{trans('general.whoops')}}!</strong> {{trans('general.text_whoops')}}.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- start: page -->
<section class="panel">
    <div class="panel-body">
        {!! Form::open(array('route' => 'settings.store','method'=>'POST')) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="tabs tabs-primary">
                    <ul class="nav nav-tabs">
                        @foreach($settingCategories as $key => $settingCategory )
                        <li class="{{$key == 0 ? 'active' : ''}}">
                            <a href="#{{$settingCategory->slug}}" data-toggle="tab" class="text-center"><i class="icons icon-check"></i> {{$settingCategory->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($settingCategories as $key => $settingCategory )
                        <div id="{{$settingCategory->slug}}" class="tab-pane {{$key == 0 ? 'active' : ''}}">
                            @foreach($settingCategory['settings'] as $key => $fields)
                            <div class="row">                                
                                <div class="col-sm-9 ">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label mt-lg">{{trans('settings.settings.'.$fields->setting_name)}}<span class="asterisk">*</span></label>
                                        <div class="col-sm-8 mt-md">
                                            @if($fields->type == 'text')
                                            <input autocomplete="off" name="{{$settingCategory->slug.'__'.$fields->setting_name}}" class="form-control" id="{{$fields->setting_name}}" value="{{old($fields->setting_name, $fields->setting_value)}}">
                                            @elseif($fields->type == 'password')
                                            <input autocomplete="new-password" type="password" name="{{$settingCategory->slug.'__'.$fields->setting_name}}" class="form-control" id="{{$fields->setting_name}}" value="{{old($fields->setting_name, $fields->setting_value)}}">
                                            @elseif($fields->type == 'email')
                                            <input autocomplete="off" type="email" name="{{$settingCategory->slug.'__'.$fields->setting_name}}" class="form-control" id="{{$fields->setting_name}}" value="{{old($fields->setting_name, $fields->setting_value)}}">
                                            @elseif($fields->type == 'image')
                                            <div class="img-setting-uploaded uploaded-{{$settingCategory->slug.'__'.$fields->setting_name}}">
                                                <img src="{{asset('assets/images/'.$fields->setting_value)}}" class="img-responsive">
                                                <div id="progress-wrp-{{$settingCategory->slug.'__'.$fields->setting_name}}" class="progress hidden">
                                                    <div class="progress-bar"></div>
                                                    <div class="status">0%</div>
                                                </div>                                                
                                            </div>
                                            {!! Form::file($settingCategory->slug.'__'.$fields->setting_name.'-file', array('class' => $settingCategory->slug.'__'.$fields->setting_name.'-file hidden')) !!}
                                            {!! Form::hidden($settingCategory->slug.'__'.$fields->setting_name, old($fields->setting_name, $fields->setting_value), array('class' => $settingCategory->slug.'__'.$fields->setting_name.'-fileuploaded')) !!}
                                            <a class="btn btn-block btn-default btn-md pt-sm pb-sm text-md upload" data-name="{{$settingCategory->slug.'__'.$fields->setting_name.'-file'}}">
                                                <i class="fa fa-upload mr-xs"></i>
                                                {{trans('general.submit')}}
                                            </a>                        

                                            @if($fields->description)
                                            <p class="help-block">{{trans('settings.settings.'.$fields->description)}}</p>
                                            @endif
                                            @elseif($fields->type == 'dropdown')
                                            <select name="{{$settingCategory->slug.'__'.$fields->setting_name}}" id="{{$fields->setting_name}}" class="form-control select">
                                                <option disabled>Select</option>
                                                @foreach($fields->getOption() as $value)
                                                <option @if($fields->setting_value == $value) selected="selected" @endif value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                            @elseif($fields->type == 'radio')
                                                @foreach($fields->getOption() as $value)
                                                <div class="col-md-6 nopadding">
                                                    <div class="rdio rdio-primary">
                                                        <input autocomplete="off" @if($fields->setting_value == $value) checked="checked" @endif name="{{$settingCategory->slug.'__'.$fields->setting_name}}" value="{{$value}}" type="radio" id="{{$fields->setting_name}}-{{$value}}">
                                                               <label for="{{$fields->setting_name}}-{{$value}}">{{ucfirst($value)}}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">{{trans('general.save')}}</button>
            </div>            
        </div>
        {!! Form::close() !!}
    </div>
</section>
@endsection

@section('javascripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
<script src="{{asset('assets/ajax/ajax-settings-upload.js')}}"></script>
<script>
var textError   = '{{trans("general.text_error")}}',
    titleError  = '{{trans("general.error")}}',
    imgError    = '{{ asset("assets/images/error.png") }}';
    $token      = '{{ csrf_token() }}';
    $url        = '{{route("settings.upload")}}';
    
</script>        
@stop
