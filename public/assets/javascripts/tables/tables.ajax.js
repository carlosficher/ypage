var paginate = null;

var tableInit = function () {

    var $table = $('#table-ajax > tbody'),
        $url   = $('#table-ajax').data('url'),
        $page  = paginate ? paginate.page : null;
    
    $.ajax({
        type: "GET",
        data: {search: $("#search").val(), list: "all", page : $page},
        url: $url,
        success: function (s) {
            if (s.total > 0) {
            paginate = {
                    current_page: s.current_page,
                    first_page_url: s.first_page_url,
                    from: s.from,
                    last_page: s.last_page,
                    last_page_url: s.last_page_url,
                    next_page_url: s.next_page_url,
                    per_page: s.per_page,
                    prev_page_url: s.prev_page_url,
                    to: s.to,
                    total: s.total
                },
                d = $(s.data);
        
            tablePaginate(paginate);
            
            $table.empty();
            for (var i = 0; i < d.length; i++) {
                var row = Object.values(d[i]);
                var tr = $('<tr>');
                for (var r = 0; r < row.length; r++) {
                    var id = row[0];
                    var td = '<td>' + row[r] + '</td>';
                    tr.append(td);

                    if (r === row.length - 1) {
                        var $action = '<td> ' +
                                '<a class="btn text-info" href="' + $url + '/' + id + '" title="' + titleShow + '"><i class="icon icon-info"></i></a>' +
                                '<a class="btn text-warning" href="' + $url + '/' + id + '/edit" title="' + titleEdit + '"><i class="icon icon-note"></i></a>' +
                                '<a class="btn text-danger" onclick="deleteRegister(' + id + ')" title="' + titleDelete + '"><i class="icon icon-trash"></i></a>' +
                                '</td>';
                        tr.append($action);
                    }
                }

                $($table).append(tr);
                $('.result-data').removeClass('hidden');
                $('#empty').addClass('hidden');
            }
            } else {
                $('.result-data').addClass('hidden');
                $('#empty').removeClass('hidden');
            }
        }
    });

};

var tablePaginate = (p) => {
    $('#table-info-from').html(p.from);
    $('#table-info-to').html(p.to);
    $('#table-info-total').html(p.total);
    
    var ulPagination = $('.pagination');
    ulPagination.empty();
    
    var liPrev = ('<li class="prev"><a onclick="prevPage(this)"><span class="fa fa-chevron-left"></span></a></li>');
    var liNext = ('<li class="next"><a onclick="nextPage(this)"><span class="fa fa-chevron-right"></span></a></li>');

    if (p.first_page_url !== p.last_page_url) {
        ulPagination.append(liPrev);
        for (var i=1; i<=p.last_page; i++) {
            var pageCurrent = $('<li class="active"><a>'+i+'</a></li>');
            var page = $('<li><a onclick="goToPage('+i+')">'+i+'</a></li>');
            if (i === p.current_page) {
                ulPagination.append(pageCurrent);
            } else {
                ulPagination.append(page);   
            }
            
            if (i === p.last_page) {
                ulPagination.append(liNext);
            }
            
            if (p.current_page === 1) {
                $('.prev').addClass('disabled');
            }
            
            if (p.current_page === p.last_page) {
                $('.next').addClass('disabled');
            }
        }
    }
};

var goToPage = (page) => {
    paginate.page = page;
    tableInit();
};

var prevPage = (e) => {
    if ( ! $(e).parent('.prev').hasClass('disabled')) {
        paginate.page = paginate.current_page - 1;
        tableInit();
    }
};

var nextPage = (e) => {
    if ( ! $(e).parent('.next').hasClass('disabled')) {
        paginate.page = paginate.current_page + 1;
        tableInit();
    }
};

$(function () {
    tableInit();
});


function deleteRegister(id) {

    var $url = $('#table-ajax').data('url');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });

    $.ajax({
        type: "DELETE",
        url: $url + '/' + id,
        success: function (s) {
            console.log(s)
        },
        error: function (e) {
            console.log(e)
        }
    });
};
