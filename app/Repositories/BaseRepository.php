<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

/**
 * Description of BaseRepository
 *
 * @author fernando
 */

use Spatie\Permission\Models\Role;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BaseRepository 
{
    use ValidatesRequests;
}
