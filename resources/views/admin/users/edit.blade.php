@extends('layouts.app')

@section('vendorstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
@stop

@section('page_heading', trans('general.management.edit_user'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('users.index')}}">
                {{trans('general.management.users')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">

        <section class="panel form-wizard" id="user">
            <div class="panel-body">
                <div class="wizard-progress wizard-progress-lg">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul class="wizard-steps">
                        <li class="active">
                            <a href="#form-account" data-toggle="tab"><span>1</span>{{trans('general.access_info')}}</a>
                        </li>
                        <li>
                            <a href="#form-profile" data-toggle="tab"><span>2</span>{{trans('general.personal_info')}}</a>
                        </li>
                        <li>
                            <a href="#form-address" data-toggle="tab"><span>3</span>{{trans('general.address')}}</a>
                        </li>
                        <li>
                            <a href="#form-phones" data-toggle="tab"><span>4</span>{{trans('general.contact_phones')}}</a>
                        </li>
                        <li>
                            <a href="#form-bank" data-toggle="tab"><span>5</span>{{trans('general.data_banks')}}</a>
                        </li>                        
                    </ul>
                </div>

                {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'class' => 'form-horizontal']) !!}
                <div class="tab-content">
                    <div id="form-account" class="tab-pane active">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <strong>{{trans('general.reference')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select('userAffiliate[parent_id]', $parentUsers,$user->userAffiliate, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                            </div>                                            
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <strong>{{trans('general.name')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('name', null, array('required', 'placeholder' => trans('general.name'),'class' => 'form-control')) !!}
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="email">
                                <strong>{{trans('general.email')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::email('email', null, array('required', 'placeholder' => trans('general.email'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="password">                                            
                                <strong>{{trans('general.password')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::password('password', array('placeholder' => trans('general.password'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="confirm-password">                                            
                                <strong>{{trans('general.confirm_password')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::password('confirm-password', array('placeholder' => trans('general.confirm_password'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="roles">                                            
                                <strong>{{trans('general.role')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select('roles[]', $roles,$userRole, array('required', 'class' => 'form-control populate', 'multiple', 'data-plugin-selectTwo')) !!}
                            </div>
                        </div>
                    </div>
                    <div id="form-profile" class="tab-pane">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <strong>{{trans('general.birth_date')}}:</strong>
                            </label>
                            <div class="col-sm-6 control-label">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon icon-calendar"></i>
                                    </span>                                
                                    {!! Form::text('userAffiliate[data_nascimento]', null, array('required', 'placeholder' => '__/__/____', 'data-plugin-masked-input', 'data-input-mask' => '99/99/9999', 'class' => 'form-control')) !!}
                                </div>
                            </div>                                            
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                <strong>{{trans('general.gender')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select('userAffiliate[sexo]', $genders, isset($user->userAffiliate->sexo) ? $user->userAffiliate->sexo : null, array('required', 'class' => 'form-control', 'data-plugin-multiselect', 'data-plugin-options' => '{ "maxHeight": 200 }')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="rg">
                                <strong>{{trans('RG')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[rg]', null, array('required', 'placeholder' => trans('RG'),'class' => 'form-control')) !!}
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="orgao_emissor">
                                <strong>{{trans('Órgão Emissor')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[orgao_emissor]', null, array('required', 'placeholder' => trans('Órgão Emissor'),'class' => 'form-control uppercase')) !!}
                            </div>                                                                        
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="cpf">
                                <strong>{{trans('CPF')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[cpf]', null, array('required', 'placeholder' => '___.___.___-__', 'data-plugin-masked-input', 'data-input-mask' => '999.999.999-99','class' => 'form-control')) !!}
                            </div>                                                                        
                        </div>                                                
                    </div>                    
                    <div id="form-address" class="tab-pane">
                        <div class="all-address">
                            @forelse($user->userAddress as $k => $address)                            
                            <div id="{{$k+1}}">                
<!--                                <div class="col-md-12">
                                    <h3 class="text-center">Endereço {{$k+1}}</h3>
                                </div>-->
                                {!! Form::hidden("userAddress[$k][id]", $address->id) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('CEP')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][cep]", $address->cep, array('required', 'placeholder' => '_____-__', 'data-plugin-masked-input', 'data-input-mask' => '99999-999','class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Logradouro')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][logradouro]", $address->logradouro, array('required', 'placeholder' => 'Logradouro', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Número')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][numero]", $address->numero, array('required', 'placeholder' => 'Número', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Complemento')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][complemento]", $address->complemento, array('required', 'placeholder' => 'Quadra, Lote, Bloco, Ap, etc...', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="bairro">
                                        <strong>{{trans('Bairro/Setor')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][bairro]", $address->bairro, array('required', 'placeholder' => 'Bairro / Setor', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cidade">
                                        <strong>{{trans('Cidade')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[$k][cidade]", $address->cidade, array('required', 'placeholder' => 'Cidade', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="uf">
                                        <strong>{{trans('Estado')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::select("userAddress[$k][uf]", $states, $address->uf, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                                    </div>                                                                        
                                </div>                                                                                
                                <hr>
                            </div>
                            @empty
                            <div id="1">                
<!--                                <div class="col-md-12">
                                    <h3 class="text-center">Endereço 1</h3>
                                </div>-->
                                {!! Form::hidden("userAddress[0][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('CEP')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][cep]", null, array('required', 'placeholder' => '_____-__', 'data-plugin-masked-input', 'data-input-mask' => '99999-999','class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Logradouro')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][logradouro]", null, array('required', 'placeholder' => 'Logradouro', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Número')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][numero]", null, array('required', 'placeholder' => 'Número', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Complemento')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][complemento]", null, array('required', 'placeholder' => 'Quadra, Lote, Bloco, Ap, etc...', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="bairro">
                                        <strong>{{trans('Bairro/Setor')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][bairro]", null, array('required', 'placeholder' => 'Bairro / Setor', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cidade">
                                        <strong>{{trans('Cidade')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][cidade]", null, array('required', 'placeholder' => 'Cidade', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="uf">
                                        <strong>{{trans('Estado')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::select("userAddress[0][uf]", $states, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                                    </div>                                                                        
                                </div>                                                                                
                                <hr>
                            </div>                            
                            @endforelse
                        </div>
<!--                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-primary add-address">{{trans('Adicionar endereço')}}</a>
                        </div>-->

                    </div>            
                    <div id="form-phones" class="tab-pane">
                        <div class="all-phones">
                            @forelse($user->userPhone as $k => $phone)
                            <div id="phone{{$k+1}}">                                        
                                {!! Form::hidden("userPhone[$k][id]", $phone->id) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{$phone->type->title}}:</strong>
                                        {!! Form::hidden("userPhone[$k][phone_type_id]", $phone->phone_type_id) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[$k][phone_number]", $phone->phone_number, array($phone->phone_type_id == 3 ? 'required' : '', 'placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[$k][contact_name]", $phone->contact_name, array($phone->phone_type_id == 3 ? 'required' : '', 'placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            @empty
                            <div id="phone1">
                                {!! Form::hidden("userPhone[0][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Residencial')}}:</strong>
                                        {!! Form::hidden("userPhone[0][phone_type_id]", 1) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[0][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[0][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone2">
                                {!! Form::hidden("userPhone[1][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="comercial">
                                        <strong>{{trans('Comercial')}}:</strong>
                                        {!! Form::hidden("userPhone[1][phone_type_id]", 2) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[1][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[1][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone3">
                                {!! Form::hidden("userPhone[2][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="celular">
                                        <strong>{{trans('Celular')}}:</strong>
                                        {!! Form::hidden("userPhone[2][phone_type_id]", 3) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[2][phone_number]", null, array('required', 'placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="celular">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[2][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone4">
                                {!! Form::hidden("userPhone[3][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('WhatsApp')}}:</strong>
                                        {!! Form::hidden("userPhone[3][phone_type_id]", 4) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[3][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[3][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            
                            @endforelse
                        </div>
                    </div>                                        
                    <div id="form-bank" class="tab-pane">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                <strong>{{trans('Banco')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select("userBank[bank_id]", $banks, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                            </div>                                                                        
                        </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Agência')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[agency]", null, array('placeholder' => 'Número da agência', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Número da conta')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[account]", null, array('placeholder' => 'Número da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Tipo da conta')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::select("userBank[account_type]", $accountTypes, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                                        
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do titular')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[holder_name]", null, array('placeholder' => 'Nome do titular da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Documento do titular')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[holder_document]", null, array('placeholder' => 'Documento do titular da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="panel-footer">
                <ul class="pager">
                    <li class="previous disabled">
                        <a><i class="fa fa-angle-left"></i> {{trans('general.prev')}}</a>
                    </li>
                    <li class="finish hidden pull-right">
                        <a>{{trans('general.finish')}}</a>
                    </li>
                    <li class="next">
                        <a>{{trans('general.next')}} <i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</div>

@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
@stop
@section('javascripts')
<script src="{{asset('assets/javascripts/users/app.js')}}"></script>
@stop