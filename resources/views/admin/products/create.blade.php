@extends('layouts.app')

@section('modalconfirmstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
@stop

@section('page_heading', trans('general.management.new_product'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('roles.index')}}">
                {{trans('general.management.products')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>{{trans('general.whoops')}}!</strong> {{trans('general.text_whoops')}}.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<!-- start: page -->
<section class="panel">
    <div class="panel-body">
        {!! Form::open(array('route' => 'products.store','method'=>'POST')) !!}
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="form-group">
                            <strong>{{trans('general.name')}}:</strong>
                            {!! Form::text('name', null, array('placeholder' => trans('general.name'),'class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <strong>{{trans('general.price')}}:</strong>
                            {!! Form::text('price', null, array('placeholder' => trans('general.price'),'class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 mt-lg">
                        <strong>{{trans('general.bonus_p')}}:</strong>
                        <div class="row mt-sm">
                            <div class="col-xs-12">
                                <label>{{trans('general.calc_type')}}:</label>
                                <div class="form-group form-inline">
                                    <div class="radio-custom radio-primary col-xs-6">
                                        {{ Form::radio('bonus_p[calc_type]', 'a', true, array('id' => 'a')) }}
                                        <label>{{ trans('general.amount') }}</label>            
                                    </div>
                                    <div class="radio-custom radio-primary col-xs-6">
                                        {{ Form::radio('bonus_p[calc_type]', 'p', false, array('id' => 'p')) }}
                                        <label>{{ trans('general.percentage') }}</label>                                            
                                    </div>                            
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <strong>{{trans('general.level1')}}:</strong>
                                    {!! Form::text('bonus_p[1]', null, array('placeholder' => trans('general.level1'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <strong>{{trans('general.level2')}}:</strong>
                                    {!! Form::text('bonus_p[2]', null, array('placeholder' => trans('general.level2'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <strong>{{trans('general.level3')}}:</strong>
                                    {!! Form::text('bonus_p[3]', null, array('placeholder' => trans('general.level3'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <strong>{{trans('general.level4')}}:</strong>
                                    {!! Form::text('bonus_p[4]', null, array('placeholder' => trans('general.level4'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <strong>{{trans('general.level5')}}:</strong>
                                    {!! Form::text('bonus_p[5]', null, array('placeholder' => trans('general.level5'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>                            
                        </div>                        
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 mt-lg">
                        <strong>{{trans('general.bonus_d')}}:</strong>
                        <div class="row mt-sm">
                            <div class="col-xs-12">
                                <label>{{trans('general.calc_type')}}:</label>
                                <div class="form-group form-inline">
                                    <div class="radio-custom radio-primary col-xs-6">
                                        {{ Form::radio('bonus_d[calc_type]', 'a', true, array('id' => 'a')) }}
                                        <label>{{ trans('general.amount') }}</label>            
                                    </div>
                                    <div class="radio-custom radio-primary col-xs-6">
                                        {{ Form::radio('bonus_d[calc_type]', 'p', false, array('id' => 'p')) }}
                                        <label>{{ trans('general.percentage') }}</label>                                            
                                    </div>                            
                                </div>
                            </div>                            
                        </div>                                                
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level1')}}:</strong>
                                    {!! Form::text('bonus_d[1]', null, array('placeholder' => trans('general.level1'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level2')}}:</strong>
                                    {!! Form::text('bonus_d[2]', null, array('placeholder' => trans('general.level2'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level3')}}:</strong>
                                    {!! Form::text('bonus_d[3]', null, array('placeholder' => trans('general.level3'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level4')}}:</strong>
                                    {!! Form::text('bonus_d[4]', null, array('placeholder' => trans('general.level4'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level5')}}:</strong>
                                    {!! Form::text('bonus_d[5]', null, array('placeholder' => trans('general.level5'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>                            
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level6')}}:</strong>
                                    {!! Form::text('bonus_d[6]', null, array('placeholder' => trans('general.level6'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level7')}}:</strong>
                                    {!! Form::text('bonus_d[7]', null, array('placeholder' => trans('general.level7'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level8')}}:</strong>
                                    {!! Form::text('bonus_d[8]', null, array('placeholder' => trans('general.level8'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level9')}}:</strong>
                                    {!! Form::text('bonus_d[9]', null, array('placeholder' => trans('general.level9'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>                            
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <strong>{{trans('general.level10')}}:</strong>
                                    {!! Form::text('bonus_d[10]', null, array('placeholder' => trans('general.level10'),'class' => 'form-control')) !!}
                                </div>                                
                            </div>                                                        
                        </div>                        
                    </div>                    
                    <div class="col-xs-12 col-sm-6 col-md-6 mt-lg">
                        <strong>{{trans('general.active')}}:</strong>
                        <div class="form-group form-inline">
                            <div class="radio-custom radio-primary col-xs-6">
                                {{ Form::radio('is_active', '1', true, array('id' => 'yes')) }}
                                <label>{{ trans('general.yes') }}</label>            
                            </div>
                            <div class="radio-custom radio-primary col-xs-6">
                                {{ Form::radio('is_active', '0', false, array('id' => 'no')) }}
                                <label>{{ trans('general.no') }}</label>                                            
                            </div>                            
                        </div>
                    </div>                    
                    
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-lg">
                        <div class="form-group">
                            <strong>{{trans('general.description')}}:</strong>
                            {!! Form::textarea('detail', null, array('placeholder' => trans('general.description'),'class' => 'form-control','style'=>'height:180px')) !!}
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class=" img-uploaded">                            
                            <img src="" class="img-responsive">
                        </div>                        
                        <div id="progress-wrp" class="hidden">
                            <div class="progress-bar"></div>
                            <div class="status">0%</div>
                        </div>                                                
                        {!! Form::file('file', array('class' => 'fileupload hidden')) !!}
                        {!! Form::hidden('photo', null, array('class' => 'uploaded')) !!}
                        <a class="btn btn-block btn-default btn-md pt-sm pb-sm text-md upload">
                            <i class="fa fa-upload mr-xs"></i>
                            {{trans('general.submit')}}
                        </a>                        
                    </div>
                </div>
            </div>            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">{{trans('general.save')}}</button>
            </div>            
        </div>
        {!! Form::close() !!}
    </div>
</section>
@endsection

@section('javascripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
<script src="{{asset('assets/ajax/ajax-upload.js')}}"></script>
<script>
var textError   = '{{trans("general.text_error")}}',
    titleError  = '{{trans("general.error")}}',
    imgError    = '{{ asset("assets/images/error.png") }}';
    $token      = '{{ csrf_token() }}';
    $url        = '{{route("products.upload")}}';
</script>        
@stop