                <div class="alert alert-danger hidden">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Erro ao salvar usuário!</strong> <br>
                    <span></span>
                </div>                
                <div class="wizard-progress wizard-progress-lg">
                    <div class="steps-progress">
                        <div class="progress-indicator"></div>
                    </div>
                    <ul class="wizard-steps">
                        <li class="active">
                            <a href="#form-account" data-toggle="tab"><span>1</span>{{trans('general.access_info')}}</a>
                        </li>
                        <li>
                            <a href="#form-profile" data-toggle="tab"><span>2</span>{{trans('general.personal_info')}}</a>
                        </li>
                        <li>
                            <a href="#form-address" data-toggle="tab"><span>3</span>{{trans('general.address')}}</a>
                        </li>
                        <li>
                            <a href="#form-phones" data-toggle="tab"><span>4</span>{{trans('general.contact_phones')}}</a>
                        </li>
                        <li>
                            <a href="#form-bank" data-toggle="tab"><span>5</span>{{trans('general.data_banks')}}</a>
                        </li>                        
                    </ul>
                </div>

                {!! Form::open(array('route' => 'network.store','method'=>'POST')) !!}
                {!! Form::hidden('roles[]', 'afiliado') !!}
                {!! Form::hidden('userAffiliate[parent_id]', null, ['id' => 'parent']) !!}
                {!! Form::hidden('userAffiliate[position_l]', null, ['id' => 'position']) !!}
                <div class="tab-content">
                    <div id="form-account" class="tab-pane active">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <strong>{{trans('general.name')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('name', null, array('required', 'placeholder' => trans('general.name'),'class' => 'form-control')) !!}
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="email">
                                <strong>{{trans('general.email')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::email('email', null, array('required', 'placeholder' => trans('general.email'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="password">                                            
                                <strong>{{trans('general.password')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::password('password', array('placeholder' => trans('general.password'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="confirm-password">                                            
                                <strong>{{trans('general.confirm_password')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::password('confirm-password', array('placeholder' => trans('general.confirm_password'),'class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div id="form-profile" class="tab-pane">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="name">
                                <strong>{{trans('general.birth_date')}}:</strong>
                            </label>
                            <div class="col-sm-6 control-label">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon icon-calendar"></i>
                                    </span>                                
                                    {!! Form::text('userAffiliate[data_nascimento]', null, array('required', 'placeholder' => '__/__/____', 'data-plugin-masked-input', 'data-input-mask' => '99/99/9999', 'class' => 'form-control')) !!}
                                </div>
                            </div>                                            
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                <strong>{{trans('general.gender')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select('userAffiliate[sexo]', $genders, null, array('required', 'class' => 'form-control', 'data-plugin-multiselect', 'data-plugin-options' => '{ "maxHeight": 200 }')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="rg">
                                <strong>{{trans('RG')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[rg]', null, array('required', 'placeholder' => trans('RG'),'class' => 'form-control')) !!}
                            </div>              
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="orgao_emissor">
                                <strong>{{trans('Órgão Emissor')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[orgao_emissor]', null, array('required', 'placeholder' => trans('Órgão Emissor'),'class' => 'form-control uppercase')) !!}
                            </div>                                                                        
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="cpf">
                                <strong>{{trans('CPF')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::text('userAffiliate[cpf]', null, array('required', 'placeholder' => '___.___.___-__', 'data-plugin-masked-input', 'data-input-mask' => '999.999.999-99','class' => 'form-control')) !!}
                            </div>                                                                        
                        </div>                                                
                    </div>                    
                    <div id="form-address" class="tab-pane">
                        <div class="all-address">
                            <div id="1">                
<!--                                <div class="col-md-12">
                                    <h3 class="text-center">Endereço 1</h3>
                                </div>-->
                                {!! Form::hidden("userAddress[0][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('CEP')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][cep]", null, array('required', 'placeholder' => '_____-__', 'data-plugin-masked-input', 'data-input-mask' => '99999-999','class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Logradouro')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][logradouro]", null, array('required', 'placeholder' => 'Logradouro', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Número')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][numero]", null, array('required', 'placeholder' => 'Número', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cep">
                                        <strong>{{trans('Complemento')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][complemento]", null, array('required', 'placeholder' => 'Quadra, Lote, Bloco, Ap, etc...', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="bairro">
                                        <strong>{{trans('Bairro/Setor')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][bairro]", null, array('required', 'placeholder' => 'Bairro / Setor', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cidade">
                                        <strong>{{trans('Cidade')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userAddress[0][cidade]", null, array('required', 'placeholder' => 'Cidade', 'class' => 'form-control')) !!}
                                    </div>                                                                        
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="uf">
                                        <strong>{{trans('Estado')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::select("userAddress[0][uf]", $states, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                                    </div>                                                                        
                                </div>                                                                                
                                <hr>
                            </div>                            
                        </div>
<!--                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <a class="btn btn-primary add-address">{{trans('Adicionar endereço')}}</a>
                        </div>-->

                    </div>            
                    <div id="form-phones" class="tab-pane">
                        <div class="all-phones">
                            <div id="phone1">
                                {!! Form::hidden("userPhone[0][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Residencial')}}:</strong>
                                        {!! Form::hidden("userPhone[0][phone_type_id]", 1) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[0][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[0][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone2">
                                {!! Form::hidden("userPhone[1][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="comercial">
                                        <strong>{{trans('Comercial')}}:</strong>
                                        {!! Form::hidden("userPhone[1][phone_type_id]", 2) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[1][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[1][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone3">
                                {!! Form::hidden("userPhone[2][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="celular">
                                        <strong>{{trans('Celular')}}:</strong>
                                        {!! Form::hidden("userPhone[2][phone_type_id]", 3) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[2][phone_number]", null, array('required', 'placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="celular">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[2][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                            <div id="phone4">
                                {!! Form::hidden("userPhone[3][id]", null) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('WhatsApp')}}:</strong>
                                        {!! Form::hidden("userPhone[3][phone_type_id]", 4) !!}
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[3][phone_number]", null, array('placeholder' => '(__) _____-____', 'data-plugin-masked-input', 'data-input-mask' => '(99) 99999-9999','class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do contato')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userPhone[3][contact_name]", null, array('placeholder' => 'Nome do contato', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                            </div>
                            <hr>
                        </div>
                    </div>                                        
                    <div id="form-bank" class="tab-pane">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">
                                <strong>{{trans('Banco')}}:</strong>
                            </label>
                            <div class="col-sm-6">
                                {!! Form::select("userBank[bank_id]", $banks, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                            </div>                                                                        
                        </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Agência')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[agency]", null, array('placeholder' => 'Número da agência', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Número da conta')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[account]", null, array('placeholder' => 'Número da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Tipo da conta')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::select("userBank[account_type]", $accountTypes, null, array('required', 'class' => 'form-control populate', 'data-plugin-selectTwo')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                                        
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Nome do titular')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[holder_name]", null, array('placeholder' => 'Nome do titular da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="fixo">
                                        <strong>{{trans('Documento do titular')}}:</strong>
                                    </label>
                                    <div class="col-sm-6">
                                        {!! Form::text("userBank[holder_document]", null, array('placeholder' => 'Documento do titular da conta', 'class' => 'form-control')) !!}
                                    </div>                                                                                                            
                                </div>                                                                                
                    </div>
                </div>
                {!! Form::close() !!}

                <ul class="pager">
                    <li class="previous disabled">
                        <a><i class="fa fa-angle-left"></i> {{trans('general.prev')}}</a>
                    </li>
                    <li class="finish hidden pull-right">
                        <a>{{trans('general.finish')}}</a>
                    </li>
                    <li class="next">
                        <a>{{trans('general.next')}} <i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>
