        <!-- Vendor -->        
        <script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
        <script src="{{asset('assets/vendor/primitive/js/jquery/jquery-ui-1.10.2.custom.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
        <script src="{{asset('assets/vendor/modernizr/modernizr.js')}}"></script>
        <script src="{{asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('assets/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>

        <!-- Specific Page Vendor -->
        <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery-appear/jquery-appear.js')}}"></script>
        <script src="{{asset('assets/vendor/owl.carousel/owl.carousel.js')}}"></script>
        <script src="{{asset('assets/vendor/isotope/isotope.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js')}}"></script>
        @yield('vendorscripts')
        @yield('modalconfirmscripts')
        <!-- Theme Base, Components and Settings -->
        <script src="{{asset('assets/javascripts/theme.js')}}"></script>

        <!-- Theme Custom -->
        <script src="{{asset('assets/javascripts/theme.custom.js')}}"></script>

        <!-- Theme Initialization Files -->
        <script src="{{asset('assets/javascripts/theme.init.js')}}"></script>

        <!-- Examples -->
        <script src="{{asset('assets/javascripts/dashboard/examples.landing.dashboard.js')}}"></script>
        <script src="{{asset('assets/javascripts/forms/examples.advanced.form.js')}}"></script>
        @yield('javascripts')