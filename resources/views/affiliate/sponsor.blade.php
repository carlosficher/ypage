@extends('layouts.app')

@section('page_heading', trans('network.laterality-list'))

@section('stylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@stop

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@can('network-create')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-default text-success" href="{{ route('network.create') }}" title="{{ trans('general.management.new_affiliate') }}"><i class="icon icon-plus fa-2x"></i> </a>            
        </div>
    </div>
</div>
@endcan

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><i class="icon icon-people"></i></h2>        
    </header>
    <div class="panel-body">
        <div class="result-data">
            <table class="table table-bordered table-striped mb-none"  id="datatable-default" >
                <thead>
                    <tr>
                        <th>ID#</th>
                        <th>{{trans('general.name')}}</th>
                        <th>{{ trans('general.email') }}</th>
                        <th>{{trans('Celular')}}</th>
                        <th>{{trans('WhatsApp')}}</th>
                        <th>{{trans('general.active')}}</th>
                        <th width="170px">{{trans('general.action')}}</th>
                    </tr>
                </thead>
                    @foreach ($userIndicated as $laterality)                    
                    <tr>
                        <td>{{ $laterality->user->id }}</td>
                        <td>{{ $laterality->user->name }}</td>
                        <td>{{ $laterality->user->email }}</td>
                        @forelse($laterality->user->userPhone as $phone)
                            @if($phone->phone_type_id === 3)
                                <td>{{ $phone->phone_number }}</td>
                            @endif
                            @if($phone->phone_type_id === 4)
                                <td>{{ $phone->phone_number }}</td>
                            @endif
                        @empty
                        <td></td>
                        <td></td>
                        @endforelse
                        <td>{!! $laterality->is_active == 0 ? '<span class="tag label label-default">Não</span>' : '<span class="tag label label-success">Sim</span>' !!} </td>
                        <td>
                            @can('network-show')
                            <a class="btn text-info" href="{{ route('lateralitys.show',$laterality->id) }}" title="{{trans("general.management.show_laterality")}}"><i class="icon icon-info"></i></a>
                            @endcan
                            @can('network-edit')
                            <a class="btn text-warning" href="{{ route('lateralitys.edit',$laterality->id) }}" title="{{trans("general.management.edit_laterality")}}"><i class="icon icon-note"></i></a>
                            @endcan
                            @can('network-delete')
                            <a class="btn text-danger modal-with-zoom-anim" href="#modalConfirm" onclick="deleteConfirm('{{$laterality->id}}', this)" title="{{trans("general.management.delete_laterality")}}"><i class="icon icon-trash"></i></a>                            
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>

@can('network-delete')
@include('components/modal-confirm')
@endcan
@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@stop

@section('javascripts')
<script src="{{asset('assets/javascripts/tables/examples.datatables.default.js')}}"></script>
@stop