<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhone extends Model
{
    
    protected  $fillable = [
        'user_id',
        'phone_type_id',
        'phone_number',
        'contact_name',        
    ];
            
    public function type()
    {
        return $this->belongsTo(PhoneType::class, 'phone_type_id', 'id');
    }
}
