@extends('layouts.app')

@section('page_heading', trans('general.management.show_product'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('products.index')}}">
                {{trans('general.management.products')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

<!-- start: page -->
<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8">
                <div class="row">                    
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.name')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        {{ $product->name }}            
                    </div>                    
                </div>
                <div class="row">                    
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.description')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        {{ $product->detail }}            
                    </div>
                </div>
                <div class="row">                  
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.price')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        R${{ number_format($product->price, 2, ',', '.') }}
                    </div>
                </div>
                <div class="row">                   
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.bonus_p')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        <div class="row">
                            @foreach( $product->bonus_p as $bonus_p )
                            <div class="col-xs-12">
                                <b>{{trans('general.level'.$bonus_p->level)}}</b>
                                {{$bonus_p->calc_type == 'a' ? 'R$' . number_format($bonus_p->value, 2, ',', '.') : 'R$' . number_format($bonus_p->value/100 * $product->price, 2, ',', '.')}}
                            </div>                            
                            @endforeach                            
                        </div>
                    </div>
                </div>
                <div class="row mt-md">                   
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.bonus_d')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        <div class="row">
                            @foreach( $product->bonus_d as $bonus_d )
                            <div class="col-xs-12">
                                <b>{{trans('general.level'.$bonus_d->level)}}</b>
                                {{$bonus_d->calc_type == 'a' ? 'R$' . number_format($bonus_d->value, 2, ',', '.') : 'R$' . number_format($bonus_d->value/100 * $product->price, 2, ',', '.')}}
                            </div>                            
                            @endforeach                            
                        </div>
                    </div>
                </div>
                <div class="row">                    
                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <strong>{{trans('general.active')}}:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-9">
                        {!! $product->is_active == 0 ? '<span class="tag label label-default">Não</span>' : '<span class="tag label label-success">Sim</span>' !!} 
                    </div>
                </div>
            </div>    
            <div class="col-xs-6 col-sm-6 col-md-4">
                <div class="col-md-12">
                    <div class=" img-uploaded">                            
                        <img src="{{asset($product->photo)}}" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
