<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Setting;
use App\Models\SettingCategory;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settingCategories = SettingCategory::with(['settings' =>
            function($q) {
                $q->where('is_active', 1)->orderBy('order', 'asc');
            }])
            ->get();
        return view('admin.settings.index', compact('settingCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->except('_token') as $key => $value) {
            $split = explode('__', $key);
            $settingCategory = SettingCategory::where('slug', $split[0])->first();
            $setting = Setting::where('setting_name', $split[1])->where('setting_category_id', $settingCategory->id)->first();
            if (!empty($setting)) {
                $setting->setting_value = $value;
                $setting->save();
            }
        }

        $settingCategories = SettingCategory::with(['settings' =>
            function($q) {
                $q->where('is_active', 1)->orderBy('order', 'asc');
            }])
            ->get();
        return redirect()->back()->with('success', 'Configurações atualizadas com sucesso!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Upload logo and favicon
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $image = $request->file('file');
            $settingName = $request->input('setting_name');
            
            if ($settingName == 'general_settings__logo') {
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/assets/images/');
            }

            if ($settingName == 'general_settings__fav') {
                $name = 'fav.png';
                $destinationPath = public_path('/assets/images/');
            }
            
            try {
                $image->move($destinationPath, $name);
                return ['filename' => url('/assets/images') . '/' . $name . '?ver=' . time()];
            } catch (Exception $ex) {
                return $ex;
            }
            
        }

    }
}
