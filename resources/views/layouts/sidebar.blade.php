                <!-- start: sidebar -->
                <aside id="sidebar-left" class="sidebar-left">

                    <div class="sidebar-header">
                        <div class="sidebar-title">
                            Menu Principal
                        </div>
                        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                        </div>
                    </div>

                    <div class="nano">
                        <div class="nano-content">
                            <nav id="menu" class="nav-main" role="navigation">

                                <ul class="nav nav-main">
                                    <li class="{{ Request::is('home*') ? 'nav-active' : '' }}">
                                        <a href="{{route('home')}}">
                                            <i class="icons icon-chart" aria-hidden="true"></i>
                                            <span>Dashboard</span>
                                        </a>                        
                                    </li>
                                    @can(['user-list'])
                                    <li class="nav-parent {{ Request::is('users*') ? 'nav-expanded nav-active' : '' }}">
                                        <a href="#">
                                            <i class="icon icon-people" aria-hidden="true"></i>
                                            <span>{{trans('general.management.users')}}</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            @can(['user-list'])
                                            <li class="{{ Request::is('users*') && !Request::is('users/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('users.index')}}">
                                                    {{trans('general.list')}}
                                                </a>
                                            </li>
                                            @endcan
                                            @can(['user-create'])
                                            <li class="{{ Request::is('users/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('users.create')}}">
                                                    {{trans('general.management.new_user')}}
                                                </a>
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                    @endcan
                                    @can(['product-list'])
                                    <li class="nav-parent {{ Request::is('products*') ? 'nav-expanded nav-active' : '' }}">
                                        <a href="#">
                                            <i class="icon icon-briefcase" aria-hidden="true"></i>
                                            <span>{{trans('general.management.products')}}</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            @can(['product-list'])
                                            <li class="{{ Request::is('products*') && !Request::is('products/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('products.index')}}">
                                                    {{trans('general.list')}}
                                                </a>
                                            </li>
                                            @endcan
                                            @can(['product-create'])
                                            <li class="{{ Request::is('products/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('products.create')}}">
                                                    {{trans('general.management.new_product')}}
                                                </a>
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                    @endcan
                                    @can(['role-list', 'role-create'])
                                    <li class="nav-parent {{ Request::is('roles*') ? 'nav-expanded nav-active' : '' }}">
                                        <a href="#">
                                            <i class="icon icon-layers" aria-hidden="true"></i>
                                            <span>{{trans('general.management.roles')}}</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            @can('role-list')
                                            <li class="{{ Request::is('roles*') && !Request::is('roles/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('roles.index')}}">
                                                    {{trans('general.list')}}
                                                </a>
                                            </li>
                                            @endcan
                                            @can('role-create')
                                            <li class="{{ Request::is('roles/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('roles.create')}}">
                                                    {{trans('general.management.new_role')}}
                                                </a>
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                    @endcan
                                    @can(['network-list'])
                                    <li class="nav-parent {{ Request::is('network*') ? 'nav-expanded nav-active' : '' }}">
                                        <a href="#">
                                            <i class="icon icon-people" aria-hidden="true"></i>
                                            <span>{{trans('general.management.network')}}</span>
                                        </a>
                                        <ul class="nav nav-children">
                                            @can('network-list')
                                            <li class="{{ Request::is('network*') && !Request::is('network/create') && !Request::is('network/distribution')? 'nav-active' : '' }}">
                                                <a href="{{route('network.laterality')}}">
                                                    {{trans('network.laterality')}}
                                                </a>
                                            </li>
                                            @endcan
                                            @can('network-list')
                                            <li class="{{ Request::is('network/distribution') ? 'nav-active' : '' }}">
                                                <a href="{{route('network.distribution')}}">
                                                    {{trans('network.distribution')}}
                                                </a>
                                            </li>
                                            @endcan                                            
                                            @can('network-create')
                                            <li class="{{ Request::is('network/create') ? 'nav-active' : '' }}">
                                                <a href="{{route('network.create')}}">
                                                    {{trans('general.management.new_affiliate')}}
                                                </a>
                                            </li>
                                            @endcan
                                        </ul>
                                    </li>
                                    @endcan
                                    @can(['network-list'])
                                    <li class="nav nav-main {{ Request::is('settings*') ? 'nav-expanded nav-active' : '' }}">
                                        <a href="{{route('settings.index')}}">
                                            <i class="icon icon-settings" aria-hidden="true"></i>
                                            <span>{{trans('general.management.settings')}}</span>
                                        </a>
                                    </li>
                                    @endcan
                                </ul>
                            </nav>
                        </div>

                        <script>
                            // Maintain Scroll Position
                            if (typeof localStorage !== 'undefined') {
                                if (localStorage.getItem('sidebar-left-position') !== null) {
                                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                                            sidebarLeft = document.querySelector('#sidebar-left .nano-content');
                                    sidebarLeft.scrollTop = initialPosition;
                                }
                            }
                        </script>

                    </div>

                </aside>
                <!-- end: sidebar -->
