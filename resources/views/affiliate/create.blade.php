@extends('layouts.app')

@section('vendorstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
@stop

@section('page_heading', trans('general.management.new_user'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('users.index')}}">
                {{trans('general.management.users')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@include('admin/users/partials/create')

@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>
@stop
@section('javascripts')
<script src="{{asset('assets/javascripts/users/app.js')}}"></script>
<script src="{{asset('assets/javascripts/pages/mediagallery.js')}}"></script>
<script src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"
    type="text/javascript" ></script>
@stop