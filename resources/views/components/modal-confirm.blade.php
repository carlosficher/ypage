@section('modalconfirmstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
@stop

<!-- Modal Confirm -->
<div id="modalConfirm" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">{{trans('general.are_you_sure')}}?</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-icon">
                    <i class="fa fa-question-circle"></i>
                </div>
                <div class="modal-text">
                    <p></p>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm">{{trans('general.confirm')}}</button>
                    <button class="btn btn-default modal-dismiss">{{trans('general.cancel')}}</button>
                </div>
            </div>
        </footer>
    </section>
</div>

@section('modalconfirmscripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>

<script>
    var text_delete         = '{{trans("general.text_are_you_sure_delete")}}' + '?',
        theId               = null, 
        titleSuccess        = '{{trans("general.success")}}',
        textSuccess         = null,        
        textDeleteSuccess   = '{{trans("general.text_delete_success")}}',
        textError           = '{{trans("general.text_error")}}',
        titleError          = '{{trans("general.error")}}',
        $token              = '{{ csrf_token() }}',
        el                  = null;

    var deleteConfirm = (id, e) => {
        $('.modal-text p').html(text_delete);
        theId = id;       
        textSuccess = textDeleteSuccess;
        el = e;
    };
    
    
(function($) {

	'use strict';
	/*
	Modal with CSS animation
	*/
	$('.modal-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,
		
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		modal: true
	});
	/*
	Modal Dismiss
	*/
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	/*
	Modal Confirm
	*/
	$(document).on('click', '.modal-confirm', function (e) {
		e.preventDefault();
                deleteRegister();
	});
        
        var deleteRegister = () => {

            var $url = $('.table').data('url') + '/' + theId;
                                    
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $token
                }
            });

            $.ajax({
                type: "DELETE",
                url: $url,
                success: function (s) {
                    $(el).closest('tr').remove();
                    $.magnificPopup.close();
                    new PNotify({
                            title: titleSuccess + '!',
                            text: textSuccess + '.',
                            type: 'success'
                    });
                    location.reload();
                },
                error: function (e) {
                    new PNotify({
                            title: titleError + '!',
                            text: textError + '. ' + e.responseJSON.message,
                            type: 'error'
                    });
                    $.magnificPopup.close();
                    location.reload();
                }
            });
        };
        
}).apply(this, [jQuery]);    
</script>
@stop
